<div id="breadcrumbs_{$block.block_id}">


{if $breadcrumbs && $breadcrumbs|@sizeof > 1}

<nav class="breadcrumb-bg">
    <div class="container no-index">
        <div class="breadcrumb">
        {strip}
            <ol>
            {foreach from=$breadcrumbs item="bc" name="bcn" key="key"}
                {if $key != "0"}
                    <!-- <span class="ty-breadcrumbs__slash">/</span> -->
                {/if}
                {if $bc.link}
                    <li><a href="{$bc.link|fn_url}" class="ty-breadcrumbs__a{if $additional_class} {$additional_class}{/if}"{if $bc.nofollow} rel="nofollow"{/if}>{$bc.title|strip_tags|escape:"html" nofilter}</a></li>
                {else}
                    <li><span class="ty-breadcrumbs__current"><bdi>{$bc.title|strip_tags|escape:"html" nofilter}</bdi></span></li>
                {/if}
            {/foreach}
            </ol>
        {/strip}
        </div>
    </div>
</nav>

{/if}

{if $breadcrumbs && $breadcrumbs|@sizeof > 1}
    <div class="ty-breadcrumbs clearfix">
        {strip}
            {include file="common/view_tools.tpl"}
        {/strip}
    </div>
{/if}
<!--breadcrumbs_{$block.block_id}--></div>
