{if $products}
    {script src="js/tygh/exceptions.js"}
    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

    {if !$no_sorting}
        {include file="views/products/components/sorting.tpl"}
    {/if}

    {if !$show_empty}
        {split data=$products size=$columns|default:"2" assign="splitted_products"}
    {else}
        {split data=$products size=$columns|default:"2" assign="splitted_products" skip_complete=true}
    {/if}

    {math equation="100 / x" x=$columns|default:"2" assign="cell_width"}
    {if $item_number == "Y"}
        {assign var="cur_number" value=1}
    {/if}

    {* FIXME: Don't move this file *}
    {script src="js/tygh/product_image_gallery.js"}

    {if $settings.Appearance.enable_quick_view == 'Y'}
        {$quick_nav_ids = $products|fn_fields_from_multi_level:"product_id":"product_id"}
    {/if}
    <div class="grid-list">
        <div class="section living-room background-none">
            <div class="container">
                <div class="tiva-row-wrap row">
                    <div class="col-md-12 col-xs-12 groupcategoriestab-vertical">
                        <div class="grouptab">
                            <div class="product-tab categoriestab-left flex-9">
                                <div class="tab-content">
                                     <div id="list" class="tab-pane fade in active show">
                                        <div class="item text-center row">
                                            {foreach from=$splitted_products item="sproducts" name="sprod"}
                                              {foreach from=$sproducts item="product" name="sproducts"}
                                                {if $product}
                                                     {assign var="obj_id" value=$product.product_id}
                                                     {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
                                                     {include file="common/product_data.tpl" product=$product show_add_to_cart=true page_status="cat_grid"}
                                                     {if $smarty.request.dispatch == 'categories.view'}
                                                         <div class="col-md-4 col-xs-12">
                                                         {else}
                                                        <div class="ty-column{$columns}">
                                                        {/if}
                                                             <div class="product-miniature js-product-miniature item-one first-item">

                                                                <div class="thumbnail-container">

                                                                    
                                                                        {include file="views/products/components/product_icon.tpl" product=$product show_gallery=true}
                                                                    
                                                                    {if isset($product.list_discount_prc) && $product.list_discount_prc > 0}
                                                                    <div class="product-flags discount">-{$product.list_discount_prc}%</div> 
                                                                    {/if}
                                                                    <div class="highlighted-informations">
                                                                        <div class="variant-links">
                                                                                <a href="#" class="color beige" title="Beige"></a>
                                                                                <a href="#" class="color orange" title="Orange"></a>
                                                                                <a href="#" class="color green" title="Green"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                               
                                                                <div class="product-description">
                                                                    <div class="product-groups">
                                                                         <div class="product-title">
                                                                              <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="product-title" title="{$product.product|strip_tags}" {live_edit name="product:product:{$product.product_id}" phrase=$product.product}>{$product.product nofilter}</a>
                                                                         </div>
                                                                         <div class="rating">
                                                                            {assign var="rating" value="rating_$obj_id"}
                                                                            <div class="grid-list__rating">
                                                                                {$smarty.capture.$rating nofilter}
                                                                            </div>
                                                                         </div>
                                                                         <div class="product-group-price">
                                                                              <div class="product-price-and-shipping">
                                                                                    <span class="price">
                                                                                        {include file="common/price.tpl" value=$product.price}
                                                                                    </span>
                                                                    
                                                                                    {if $product.list_price > 0}
                                                                                        <del>
                                                                                        {include file="common/price.tpl" value=$product.price}</del>
                                                                                    {/if}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="ty-quick-view-button__wrapper"> 
                                                                         {assign var="form_open" value="form_open_`$obj_id`"}
                                                                         {$smarty.capture.$form_open nofilter}
                                                                        {hook name="products:product_multicolumns_list"}
                                                                         <div class="ty-grid-list__control">
                                                                             {if $settings.Appearance.enable_quick_view == 'Y'}
                                                                                {include file="views/products/components/quick_view_link.tpl" quick_nav_ids=$quick_nav_ids}
                                                                            {/if}
                                                                           
                                                                                 {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                                                                                 {$smarty.capture.$add_to_cart nofilter}
                                                                       
                                                                          
                                                                         </div>
                                                                        {/hook}
                                                                    </div>
                                                                   <div style="display:none;" class="product-buttons d-flex justify-content-center">
                                                                            {assign var="form_open" value="form_open_`$obj_id`"}
                                                                            {$smarty.capture.$form_open nofilter}
                                                                            
                                                                            {include file="buttons/button.tpl" but_id=$but_id but_name=$but_name but_onclick=$but_onclick but_href=$but_href but_target=$but_target but_role="text" but_meta="product-button ty-quick-view-button ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer add-to-cart" but_icon="fa fa-shopping-cart"}

                                                                                <input type="hidden" name="token">
                                                                                
                                                                            {assign var="form_close" value="form_close_`$obj_id`"}
                                                                            {$smarty.capture.$form_close nofilter}
                                                                 </div>
                                                             </div>
                                                        </div>
                                                     </div>
                                                {/if}
                                            {/foreach}
                                        {/foreach}
                                     </div>
                                </div>
                             </div>
                        </div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
 </div>
    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

{/if}
{literal}
<style>
.addToWishlist{
  opacity: 0;
  visibility: visible;
  display: inline-block;
  width: 38px;
  height: 38px;
  text-align: center;
  line-height: 38px;
  border-radius: 3px;
  border: 1px solid #ddd;
  background: #fff;
  bottom: -40px;
  position: relative;
  transition: all 0.3s ease;
  margin-right: 10px;
}
.addToWishlist:hover{
  background: #878787;
  border: 1px solid #878787;
}
.addToWishlist:hover i{
     color: #fff;
}
.add-to-cart {
  width: 38px;
  height: 38px;
  text-align: center;
  line-height: 38px;
  border-radius: 3px;
  background: #fff;
  border: 1px solid #ddd;
  display: inline-block;
  margin-right: 10px;
}
.add-to-cart:hover {
  background: #878787;
  border: 1px solid #878787;
}
.add-to-cart:hover i {
  color: #fff;
}
.quick-view {
  opacity: 0;
  visibility: visible;
  display: inline-block;
  width: 38px;
  height: 38px;
  text-align: center;
  line-height: 38px;
  border-radius: 3px;
  border: 1px solid #ddd;
  background: #fff;
  bottom: -40px;
  position: relative;
  transition: all 0.3s ease;
  margin-right: 10px;
}
.quick-view:hover {
  background: #878787;
  border: 1px solid #878787;
}
 .quick-view:hover i {
  color: #fff;
}
.ty-btn i {
    bottom: 10px;
    right: 6px;
        top: auto;
    margin-right: auto;
    }
    .ty-stars{
        margin-right: auto;
    }
    .owl-carousel .owl-wrapper-outer {
          height: auto;
    }
    .fa-eye{
    color:black;
    }

</style>
{/literal}
{capture name="mainbox_title"}{$title}{/capture}