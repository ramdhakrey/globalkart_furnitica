{hook name="buttons:add_to_cart"}
    {assign var="c_url" value=$config.current_url|escape:url}
    {if $settings.General.allow_anonymous_shopping == "allow_shopping" || $auth.user_id}
       {if isset($page_status) && $page_status=='yes'}
            {include file="buttons/button.tpl" but_id=$but_id but_name=$but_name but_onclick=$but_onclick but_href=$but_href but_target=$but_target but_role="text" but_meta="ty-quick-view-button product-button quick-view hidden-sm-down ty-btn ty-btn__secondary ty-btn__big y-btn__add-to-cart cm-form-dialog-closer add-to-cart" but_icon="fa fa-shopping-cart"}
        {else}
            {include file="buttons/button.tpl" but_id=$but_id but_name=$but_name but_onclick=$but_onclick but_href=$but_href but_target=$but_target but_role="text" but_meta="add_product_cart_list product-button ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer add-to-cart" but_icon="fa fa-shopping-cart" } 
         {/if}
    {else}

        {if $runtime.controller == "auth" && $runtime.mode == "login_form"}
            {assign var="login_url" value=$config.current_url}
        {else}
            {assign var="login_url" value="auth.login_form?return_url=`$c_url`"}
        {/if}

        {include file="buttons/button.tpl" but_id=$but_id but_text=__("sign_in_to_buy") but_href=$login_url but_role=$but_role|default:"text" but_name=""}
        <p>{__("text_login_to_add_to_cart")}</p>
    {/if}
{/hook}
