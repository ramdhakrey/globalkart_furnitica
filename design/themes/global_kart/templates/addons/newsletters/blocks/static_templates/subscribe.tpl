{** block-description:tmpl_subscription **}
{if $addons.newsletters}
<div class="section newsletter">
                            <div class="container">
                                <div class="row">
                                    <div class="news-content">
                                        <div class="tiva-modules">
                                            <div class="block">
                                                <div class="title-block">Newsletter</div>
                                                <div class="sub-title">Sign up to our newsletter to get the latest articles, lookbooks voucher codes
                                                    direct to your inbox</div>
                                                <div class="block-newsletter">
                                                    <form action="{""|fn_url}" method="post" name="subscribe_form" class="cm-processing-personal-data">
                                                        <input type="hidden" name="redirect_url" value="{$config.current_url}" />
                                                        <input type="hidden" name="newsletter_format" value="2" />

                                                        {* <h3 class="ty-footer-form-block__title">{__("stay_connected")}</h3> *}

                                                        {hook name="newsletters:email_subscription_block"}

                                                        <div class="ty-footer-form-block__form-container">
                                                            <div class="ty-footer-form-block__form ty-control-group ty-input-append cm-block-add-subscribe">
                                                                <label class="cm-required cm-email hidden" for="subscr_email{$block.block_id}">{__("email")}</label>
                                                                <input type="text" name="subscribe_email" id="subscr_email{$block.block_id}" size="20" value="{__("enter_email")}" class="cm-hint ty-input-text" />
                                                                <span class="input-group-btn">
                                                                {include file="buttons/news_go.tpl" but_name="newsletters.add_subscriber" alt=__("go")}
                                                                </span>
                                                            </div>
                                                        </div>

                                                        {/hook}
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="block">
                                                <div class="social-content">
                                                    <div id="social-block">
                                                        <div class="social">
                                                            <ul class="list-inline mb-0 justify-content-end">
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-google"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="list-inline-item mb-0">
                                                                    <a href="#" target="_blank">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Popup newsletter -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{/if}