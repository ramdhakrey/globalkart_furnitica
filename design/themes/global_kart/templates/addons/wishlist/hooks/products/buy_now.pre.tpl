
{if $smarty.request.dispatch == 'categories.view'}
{if !$hide_wishlist_button}
    {include file="addons/wishlist/views/wishlist/components/add_to_wishlist.tpl" but_id="button_wishlist_`$obj_prefix``$product.product_id`" but_name="dispatch[wishlist.add..`$product.product_id`]" but_role="text" but_meta="addToWishlist" but_icon="fa"}
{/if}
 <a class="addToquicktproduct ty-quick-view-button product-button  ty-btn ty-btn__secondary ty-btn__big cm-dialog-opener cm-dialog-auto-size" data-ca-view-id="{$product.product_id}" data-ca-target-id="product_quick_view" href="{$quick_view_url|fn_url}" data-ca-dialog-title="{__("quick_product_viewer")}" rel="nofollow"><i class="fa fa-eye" aria-hidden="true"></i></a>
 {else}
  {include file="addons/wishlist/views/wishlist/components/add_to_wishlist.tpl" but_id="button_wishlist_`$obj_prefix``$product.product_id`" but_name="dispatch[wishlist.add..`$product.product_id`]" but_role="text" but_meta="addToWishlist" but_icon="fa fa-heart"}
 {/if}