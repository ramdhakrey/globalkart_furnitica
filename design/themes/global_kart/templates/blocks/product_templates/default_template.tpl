{script src="js/tygh/exceptions.js"}

<!-- yoo -->

<div class="col-sm-8 col-lg-9 col-md-9">

{hook name="products:view_main_info"}
        {if $product}
        {assign var="obj_id" value=$product.product_id}
        {include file="common/product_data.tpl" product=$product but_role="big" but_text=__("add_to_cart") quantity_text="Qty"}

                                        <div class="main-product-detail">

                                        {assign var="form_open" value="form_open_`$obj_id`"}
                                        {$smarty.capture.$form_open nofilter}
                {if !$hide_title}
                        <h2 class="custom_h2">{$product.product nofilter}</h2>
                {/if}

                
                                
                                            <div class="product-single row">
                                                <div class="product-detail col-xs-12 col-md-5 col-sm-5">
                                                    <div class="page-content" id="content">
                                                        <div class="images-container">
                                                            <div class="js-qv-mask mask tab-content border">
                                                                <div id="item1" class="tab-pane fade active in show">
                                                                       {hook name="products:image_wrap"}
                                                                            {if !$no_images}
                                                                                <div class="ty-product-block__img cm-reload-{$product.product_id}" id="product_images_{$product.product_id}_update">

                                                                                    {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}
                                                                                    {$smarty.capture.$discount_label nofilter}

                                                                                    {include file="views/products/components/product_images.tpl" product=$product show_detailed_link="Y" image_width=$settings.Thumbnails.product_details_thumbnail_width image_height=$settings.Thumbnails.product_details_thumbnail_height}
                                                                                <!--product_images_{$product.product_id}_update--></div>
                                                                            {/if}
                                                                        {/hook}
                                                                </div>
                                                                <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal">
                                                                    <i class="fa fa-expand"></i>
                                                                </div>
                                                            </div>
                                                            <div class="modal fade" id="product-modal" role="dialog">
                                                                <div class="modal-dialog">

                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <div class="modal-body">
                                                                                <div class="product-detail">
                                                                                    <div>
                                                                                        <div class="images-container">
                                                                                            <div class="js-qv-mask mask tab-content">
                                                                                                <div id="modal-item1" class="tab-pane fade active in show">
                                                                                                    <img src="img/product/1.jpg" alt="img">
                                                                                                </div>
                                                                                                <div id="modal-item2" class="tab-pane fade">
                                                                                                    <img src="img/product/2.jpg" alt="img">
                                                                                                </div>
                                                                                                <div id="modal-item3" class="tab-pane fade">
                                                                                                    <img src="img/product/3.jpg" alt="img">
                                                                                                </div>
                                                                                                <div id="modal-item4" class="tab-pane fade">
                                                                                                    <img src="img/product/5.jpg" alt="img">
                                                                                                </div>
                                                                                            </div>
                                                                                            <ul class="product-tab nav nav-tabs">
                                                                                                <li class="active">
                                                                                                    <a href="#modal-item1" data-toggle="tab" class=" active show">
                                                                                                        <img src="img/product/1.jpg" alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#modal-item2" data-toggle="tab">
                                                                                                        <img src="img/product/2.jpg" alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#modal-item3" data-toggle="tab">
                                                                                                        <img src="img/product/3.jpg" alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <a href="#modal-item4" data-toggle="tab">
                                                                                                        <img src="img/product/5.jpg" alt="img">
                                                                                                    </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-info col-xs-12 col-md-7 col-sm-7">
                                                    <div class="detail-description">
                                                        <div class="price-del">
                                                            
                                                            {assign var="old_price" value="old_price_`$obj_id`"}
                                                            {assign var="price" value="price_`$obj_id`"}
                                                            {assign var="clean_price" value="clean_price_`$obj_id`"}
                                                            {assign var="list_discount" value="list_discount_`$obj_id`"}
                                                            {assign var="discount_label" value="discount_label_`$obj_id`"}

                                                            {hook name="products:promo_text"}
                                                            {if $product.promo_text}
                                                            <div class="ty-product-block__note-wrapper">
                                                                <div class="ty-product-block__note ty-product-block__note-inner">
                                                                    {$product.promo_text nofilter}
                                                                </div>
                                                            </div>
                                                            {/if}
                                                            {/hook}


                                                            
                                                                <span class="price">{include file="common/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="ty-price-num" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}</span>
                                                                {if $product.amount > 0}
                                                                <span class="float-right custom_avail_position">
                                                                    <span class="availb">Availability: </span>
                                                                    <span class="check">
                                                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>IN STOCK</span>
                                                                </span>
                                                                {/if}

                                                            {* {assign var="qty" value="qty_`$obj_id`"}
                                                            {$smarty.capture.$qty nofilter}

                                                            {assign var="min_qty" value="min_qty_`$obj_id`"}
                                                            {$smarty.capture.$min_qty nofilter} *}

                                                        </div>
                                                        <p class="description">{if $product.short_description != ''}
                                                            {$product.short_description|strip_tags}
                                                        {/if}</p>
                                                        <div class="option has-border d-lg-flex size-color">
                                                            {assign var="product_options" value="product_options_`$obj_id`"}
                                                            {$smarty.capture.$product_options nofilter}
                                                        </div>
                                                        <div class="has-border cart-area row">
                                                            <div class="product-quantity">
                                                                
                                                                {if $capture_buttons}{capture name="buttons"}{/if}
                                                                    <div class="ty-product-block__button quantity">
                                                                        {if $show_details_button}
                                                                            {include file="buttons/button.tpl" but_href="products.view?product_id=`$product.product_id`" but_text=__("view_details") but_role="submit"}
                                                                        {/if}

                                                                        {assign var="qty" value="qty_`$obj_id`"}
                                                                        {$smarty.capture.$qty nofilter}

                                                                        {assign var="min_qty" value="min_qty_`$obj_id`"}
                                                                        {$smarty.capture.$min_qty nofilter}

                                                                        {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                                                                        {$smarty.capture.$add_to_cart nofilter}

                                                                        {assign var="list_buttons" value="list_buttons_`$obj_id`"}
                                                                        {$smarty.capture.$list_buttons nofilter}
                                                                    </div>
                                                                {if $capture_buttons}{/capture}{/if}
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <p class="product-minimal-quantity">
                                                            </p>
                                                        </div>
                                                        <div class="d-flex2 has-border">
                                                            <div class="btn-group">
                                                               {hook name="products:product_detail_bottom"}
                                                               {/hook}
                                                            </div>
                                                        </div>

                                                          <div class="content">
                                                            {hook name="products:main_info_title"}
                                                            {/hook}
                                                             <p>SKU :
                                                                <span class="content2">
                                                                    <a href="#">{$product.product_code}</a>
                                                                </span>
                                                            </p>
                                                            <p>Categories :
                                                                <span class="content2">
                                                                {foreach from=$product.category_ids item=category_id}
                                                                    <a href="#">{$category_id|fn_get_category_name}</a>
                                                                {/foreach}
                                                                </span>
                                                            </p>
                                                            {if $product.tags}
                                                            <p>tags :
                                                                <span class="content2">
                                                                {foreach from=$product.tags item=tag}
                                                                    <a href="#">{$tag.tag}</a>
                                                                {/foreach}
                                                                </span>
                                                            </p>
                                                            {/if}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="review">
                                                {if $show_product_tabs}
                                                    {hook name="products:product_tabs"}
                                                        {include file="views/tabs/components/product_tabs.tpl"}

                                                        {if $blocks.$tabs_block_id.properties.wrapper}
                                                            {include file=$blocks.$tabs_block_id.properties.wrapper content=$smarty.capture.tabsbox_content title=$blocks.$tabs_block_id.description}
                                                        {else}
                                                            {$smarty.capture.tabsbox_content nofilter}
                                                        {/if}
                                                    {/hook}
                                                {/if}
                                            </div>
                                          <div class="related">
                                                <div class="title-tab-content  text-center">
                                                    <div class="title-product justify-content-start">
                                                        <h2>Related Products</h2>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-content">
                                                    <div class="row">
                                                        <div class="item text-center col-md-4">
                                                            <div class="product-miniature js-product-miniature item-one first-item">
                                                                <div class="thumbnail-container border border">
                                                                    <a href="product-detail.html">
                                                                        <img class="img-fluid image-cover" src="img/product/1.jpg" alt="img">
                                                                        <img class="img-fluid image-secondary" src="img/product/22.jpg" alt="img">
                                                                    </a>
                                                                    <div class="highlighted-informations">
                                                                        <div class="variant-links">
                                                                            <a href="#" class="color beige" title="Beige"></a>
                                                                            <a href="#" class="color orange" title="Orange"></a>
                                                                            <a href="#" class="color green" title="Green"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="product-description">
                                                                    <div class="product-groups">
                                                                        <div class="product-title">
                                                                            <a href="product-detail.html">Nulla et justo non augue</a>
                                                                        </div>
                                                                        <div class="rating">
                                                                            <div class="star-content">
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="product-group-price">
                                                                            <div class="product-price-and-shipping">
                                                                                <span class="price">£28.08</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="product-buttons d-flex justify-content-center">
                                                                        <form action="index.html" method="post" class="formAddToCart">
                                                                            <a class="add-to-cart" href="#" data-button-action="add-to-cart">
                                                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                            </a>
                                                                        </form>
                                                                        <a class="addToWishlist" href="#" data-rel="1" onclick="">
                                                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                                                        </a>
                                                                        <a href="#" class="quick-view hidden-sm-down" data-link-action="quickview">
                                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item text-center col-md-4">
                                                            <div class="product-miniature js-product-miniature item-one first-item">
                                                                <div class="thumbnail-container border">
                                                                    <a href="product-detail.html">
                                                                        <img class="img-fluid image-cover" src="img/product/2.jpg" alt="img">
                                                                        <img class="img-fluid image-secondary" src="img/product/11.jpg" alt="img">
                                                                    </a>
                                                                    <div class="highlighted-informations">
                                                                        <div class="variant-links">
                                                                            <a href="#" class="color beige" title="Beige"></a>
                                                                            <a href="#" class="color orange" title="Orange"></a>
                                                                            <a href="#" class="color green" title="Green"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="product-description">
                                                                    <div class="product-groups">
                                                                        <div class="product-title">
                                                                            <a href="product-detail.html">Nulla et justo non augue</a>
                                                                        </div>
                                                                        <div class="rating">
                                                                            <div class="star-content">
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="product-group-price">
                                                                            <div class="product-price-and-shipping">
                                                                                <span class="price">£31.08</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="product-buttons d-flex justify-content-center">
                                                                        <form action="index.html" method="post" class="formAddToCart">
                                                                            <a class="add-to-cart" href="#" data-button-action="add-to-cart">
                                                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                            </a>
                                                                        </form>
                                                                        <a class="addToWishlist" href="#" data-rel="1" onclick="">
                                                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                                                        </a>
                                                                        <a href="#" class="quick-view hidden-sm-down" data-link-action="quickview">
                                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item text-center col-md-4">
                                                            <div class="product-miniature js-product-miniature item-one first-item">
                                                                <div class="thumbnail-container border">
                                                                    <a href="product-detail.html">
                                                                        <img class="img-fluid image-cover" src="img/product/3.jpg" alt="img">
                                                                        <img class="img-fluid image-secondary" src="img/product/14.jpg" alt="img">
                                                                    </a>
                                                                    <div class="highlighted-informations">
                                                                        <div class="variant-links">
                                                                            <a href="#" class="color beige" title="Beige"></a>
                                                                            <a href="#" class="color orange" title="Orange"></a>
                                                                            <a href="#" class="color green" title="Green"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="product-description">
                                                                    <div class="product-groups">
                                                                        <div class="product-title">
                                                                            <a href="product-detail.html">Nulla et justo non augue</a>
                                                                        </div>
                                                                        <div class="rating">
                                                                            <div class="star-content">
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                                <div class="star"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="product-group-price">
                                                                            <div class="product-price-and-shipping">
                                                                                <span class="price">£20.08</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="product-buttons d-flex justify-content-center">
                                                                        <form action="index.html" method="post" class="formAddToCart">
                                                                            <a class="add-to-cart" href="#" data-button-action="add-to-cart">
                                                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                                            </a>
                                                                        </form>
                                                                        <a class="addToWishlist" href="#" data-rel="1" onclick="">
                                                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                                                        </a>
                                                                        <a href="#" class="quick-view hidden-sm-down" data-link-action="quickview">
                                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                   

    {/if}

    {/hook}
                                    </div>

<!-- yoo end -->
