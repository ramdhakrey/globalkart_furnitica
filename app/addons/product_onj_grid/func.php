<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;
use Tygh\Navigation\LastView;

// function fn_product_onj_grid_get_product_data_post(&$product_data, $auth, $preview, $lang_code)
// {
//     Custom, We always select default template as for the theme needs
//     if (isset($product_data["details_layout"]) && $product_data["details_layout"] != 'default')
//         $product_data["details_layout"] = 'default';

// }

function fn_product_onj_grid_get_product_details_views_post(&$product_details_views, $get_default)
{
    $controller = '';
    $mode = '';
    if (isset($_REQUEST["dispatch"])) {
        $dispatchArr = explode('.', $_REQUEST["dispatch"]);
        $controller = $dispatchArr[0];
        $mode = $dispatchArr[1];
    }

    /*Custom, We always select default template as for the theme needs*/
    // if ($controller == 'products' && $mode == 'update') {
        $product_details_views = array();
        $product_details_views["default"] = 'Parent (Default template)';
    // }

}